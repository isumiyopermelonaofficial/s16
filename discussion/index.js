// console.log("Hello World");
/*
	3 Types of looping constructs
	1. While Loop
	2. Do-while lopp
	3. for loop

*/
/*
	While loop
	- Syntax
		While(expression/condition){
			statement/s;
		}
*/
let count = 1;

while(count <= 5){
	console.log("While: " + count);
	++count;
}

// Do While Loop

/*
	Do While

		Syntax:
			do{
				statement;
			}while(expression/condition);

*/

let num1 = 1;

console.log("Do-While Statement");
do{
	console.log("Do-While: " + num1);
	num1++;
}while(num1 < 6)


let num2 = Number(prompt("Give me a number: "));
console.log("New Number Entry");
do{
	console.log("Do While: " + num2);
	num2 += 1;
}while(num2 < 10)


// For Loop

/*
	For Loop

	Syntax:
		for(initialization; expression/condition; finalExpression){
			statement;
		}
*/

// console.log("For Loop");
// for(let num3 = 1; num3 <= 5; num3++){
// 	console.log(num3);
// }

// console.log("For loop with prompt: ");
// let newNum = Number(prompt("Enter a number"));

// for(let num4 = newNum; num4 < 10; num++){
// 	console.log(num4);
// }

// Mini Activity
console.log("Display a Series of number");
let userNum = Number(prompt("Enter a number"));

for(let num4 = 1; num4 <= userNum; num4++){
	console.log(num4);
}

let myString = "Juan";
console.log(myString.length);

console.log(myString[0]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Displaying Individual letters in a string");

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

let myName = "AloNzo";

for(let i = 0; i < myName.length; i++){
	// if the character of the name is a vowel, instead of displaying the character, display number 3;

	if(myName[i].toLowerCase() == 'a' || 
	   myName[i].toLowerCase() == 'e' ||
	   myName[i].toLowerCase() == 'i' ||
	   myName[i].toLowerCase() == 'o' ||
	   myName[i].toLowerCase() == 'u'
	   )
	{
		console.log(3);
	}

	else
	{
		console.log(myName[i]);
	}
}

console.log("Continue and Break");

/*
	Continue and Break statements
	 - Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
	 - Break statement is used to terminate the current loop once a match has been found.
*/

for(let count = 0; count <= 20; count++){
	if(count% 2 === 0){
		continue;
	}
	console.log("Continue and Break: " + count);

	if(count > 10){
		break;
	}
}